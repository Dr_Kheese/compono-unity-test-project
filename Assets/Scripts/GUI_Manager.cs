﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUI_Manager : MonoBehaviour
{
    //Make sure to attach these Buttons in the Inspector
    public Button m_buttonPrev;
    public Button m_buttonNext;
    public Button m_buttonPlay;
    public Button m_buttonExit;
    public Slider m_slider;

    public ExportRoutine m_exportRoutine;

    void Start( )
    {
        //Calls the TaskOnClick/TaskWithParameters/ButtonClicked method when you click the Button
        m_buttonPrev.onClick.AddListener( ButtonPrevClicked );
        m_buttonNext.onClick.AddListener( ButtonNextClicked );
        m_buttonPlay.onClick.AddListener( ButtonPlayClicked );
        m_buttonExit.onClick.AddListener( ButtonExitClicked );
        m_slider.onValueChanged.AddListener( SliderValueChanged );

    }



    void ButtonPrevClicked( )
    {
        m_exportRoutine.OnButtonPrev( );
    }



    void ButtonNextClicked( )
    {
        m_exportRoutine.OnButtonNext( );
    }



    void ButtonPlayClicked( )
    {
        m_exportRoutine.OnButtonPlay( );
    }



    void ButtonExitClicked( )
    {
        m_exportRoutine.OnButtonExit( );
    }



    void SliderValueChanged( float a_value )
    {
        m_exportRoutine.OnSliderValueChanged( a_value );
    }

}
