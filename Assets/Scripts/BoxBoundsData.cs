﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBoundsData
{
    public float m_XMax;
    public float m_XMin;

    public float m_YMax;
    public float m_YMin;

    public float m_ZMax;
    public float m_ZMin;
}
