﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class CameraCapture : MonoBehaviour
{
    private string m_pathOutput;

    public Camera m_cameraView;
    private Camera m_cameraRender;



    public void SetOrthographicSize( float a_size )
    {
        m_cameraRender.orthographicSize = a_size;
        m_cameraView.orthographicSize = a_size;
    }



    public void Start( )
    {
        if( !m_cameraRender )
        {
            m_cameraRender = this.GetComponent<Camera>( );
        }

        m_pathOutput =  Application.dataPath + "/../Output/";
    }


    public void Capture( string a_objectName, byte a_imageIndex )
    {
        RenderTexture activeRenderTexture = RenderTexture.active;
        RenderTexture.active = m_cameraRender.targetTexture;

        m_cameraRender.Render( );

        //Debug.Log( "Camera width: " + m_cameraRender.targetTexture.width + ", Camera height: " + m_cameraRender.targetTexture.height );

        Texture2D image = new Texture2D(m_cameraRender.targetTexture.width, m_cameraRender.targetTexture.height);
        image.ReadPixels( new Rect( 0, 0, m_cameraRender.targetTexture.width, m_cameraRender.targetTexture.height ), 0, 0 );
        image.Apply( );
        RenderTexture.active = activeRenderTexture;

        byte[] bytes = image.EncodeToPNG();
        Destroy( image );



        // Setup file name.
        string _PathFile = m_pathOutput + a_objectName;


        // Ensure output directory exists.
        if( !Directory.Exists( m_pathOutput ) )
        {
            Directory.CreateDirectory( m_pathOutput );
        }

        // Ensure prefab directory exists.
        if( !Directory.Exists( _PathFile ) )
        {
            Directory.CreateDirectory( _PathFile );
        }


        // Save image data to file.
        string _NameImage = _PathFile + "/frame" + a_imageIndex.ToString( "D4" ) + ".png";
        File.WriteAllBytes( _NameImage, bytes );

        //Debug.Log( "Saving Image " + _NameImage + " to file" );
    }
}
