﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Collections.Generic;

public class ExportRoutine : MonoBehaviour
{
    [SerializeField] private SpawnItemList m_itemList = null;

    private AssetReferenceGameObject m_assetLoadedAsset;
    private GameObject m_instanceObject = null;

    private Transform m_transform;

    public CameraCapture m_cameraCapture;

    public float m_rotationPeriod = 6.0f;
    private float m_rotationPeriodMin = 0.5f;
    private float m_rotationPeriodMax = 30.0f;

    private float m_angleIncrement = 22.5f;
    private float m_angleChangeSinceLastImage;
    private byte m_angleIndex;
    private byte m_angleIndexMax = 15;

    private byte m_indexItem;

    private bool m_paused = false;



    private void Start( )
    {
        if( m_itemList != null && m_itemList.AssetReferenceCount > 0 )
        {
            LoadItemAtIndex( m_itemList, m_indexItem );
        }
        else
        {
            Debug.LogError( "Spawn list not setup correctly" );

        }

    }



    private void Stop( )
    {
        // Close down the program.
#if UNITY_STANDALONE
        //Quit the application
        Application.Quit( );

#endif
        //If we are running in the editor
#if UNITY_EDITOR
        //Stop playing the scene
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }



    private void ResetAngleData( )
    {
        m_angleChangeSinceLastImage = m_angleIncrement;
        m_angleIndex = 0;
    }



    public void Update( )
    {
        if( !m_paused )
        {

            m_rotationPeriod = Mathf.Clamp( m_rotationPeriod, m_rotationPeriodMin, m_rotationPeriodMax );

            // Setup object turn-around.
            if( m_instanceObject != null )
            {

                float _RotationAngle = 360.0f / m_rotationPeriod * Time.deltaTime;

                // Check if at appropriate angle to capture image.
                float _angleCurrent = m_transform.rotation.eulerAngles.y;

                //Debug.Log( "Angle current: " + _angleCurrent + ", Angle modulus: " + _angleCurrent % m_angleIncrement );
                if( m_angleIndex <= m_angleIndexMax )  // Ensure that image capture only occurs for unique increments of angle.
                {
                    if( m_angleChangeSinceLastImage >= ( m_angleIncrement / 2.0f ) // Deadzone to ensure each angle increment is only recorded once.
                        && _angleCurrent % m_angleIncrement < _RotationAngle * 2.0f ) // Only capture an image once the object has rotated to at least the next angle increment.
                    {
                        //Debug.Log( m_instanceObject.name + " is at angle [ " + _angleCurrent + " ]: setting object to nearest capture angle." );

                        // Snap object to exact angle increment.
                        float _angleNew = m_angleIncrement * m_angleIndex;

                        //Debug.Log( "New angle: " + _angleNew );

                        m_transform.rotation = Quaternion.AngleAxis( _angleNew, Vector3.up );

                        _angleCurrent = m_transform.rotation.eulerAngles.y;

                        //Debug.Log( m_instanceObject.name + " is at angle [ " + _angleCurrent + " ]: capturing image." );

                        // Capture image.
                        m_cameraCapture.Capture( m_instanceObject.name, m_angleIndex );

                        // Setup for next image.
                        m_angleIndex++;
                        m_angleChangeSinceLastImage = 0.0f;

                        //Debug.Log( "New angle index: " + m_angleIndex );
                    }
                }

                // Rotate object.
                m_instanceObject.transform.Rotate( Vector3.up, _RotationAngle );
                m_angleChangeSinceLastImage += _RotationAngle;

                //Debug.Log( "Rotation increment this frame: " + _RotationAngle + ", New angle: " + _angleCurrent );

                // Check if object has finished its turn-around.
                if( m_angleIndex >= m_angleIndexMax && _angleCurrent < m_angleIncrement )
                {
                    // Check if there are more items to load.
                    if( m_indexItem < m_itemList.AssetReferenceCount - 1 )
                    {
                        m_indexItem++;
                        LoadItemAtIndex( m_itemList, m_indexItem );
                    }
                    else
                    {
                        // No more prefabs to show.
                        // Close down the program.
                        Stop( );
                    }
                }
            }
        }
    }

    private void LoadItemAtIndex( SpawnItemList itemList, int index )
    {
        if( m_instanceObject != null )
        {
            Destroy( m_instanceObject );
        }

        m_assetLoadedAsset = itemList.GetAssetReferenceAtIndex( index );
        var spawnPosition = new Vector3();
        var spawnRotation = Quaternion.identity;
        var parentTransform = this.transform;


        var loadRoutine = m_assetLoadedAsset.LoadAssetAsync();
        loadRoutine.Completed += LoadRoutine_Completed;

        void LoadRoutine_Completed( UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj )
        {
            m_instanceObject = Instantiate( obj.Result, spawnPosition, spawnRotation, parentTransform );

            m_transform = m_instanceObject.transform;

            // Find the biggest box.  This will be used to centre the object within the camera frame.
            float _DimensionHighestMagnitude = 0.0f;

            // Add box colliders to everything for use in determining position within camera frame.
            var _boxColliderPrime = m_instanceObject.AddComponent<BoxCollider>( );

            var _boxBoundsData = new BoxBoundsData();
            BoxCollider _boxColliderBiggest = _boxColliderPrime;
            FindBiggestCollider( ref _boxColliderBiggest, _boxColliderPrime, ref _DimensionHighestMagnitude, ref _boxBoundsData );


            foreach( Transform _transform in m_transform )
            {
                var _boxColliderTemp = _transform.gameObject.AddComponent<BoxCollider>( );

                if( _boxColliderTemp != null )
                {
                    FindBiggestCollider( ref _boxColliderBiggest, _boxColliderTemp, ref _DimensionHighestMagnitude, ref _boxBoundsData );
                }
            }

            if( _boxColliderBiggest != null )
            {
                // Position the object in the centre of the frame using the largest collider.
                m_transform.position -= _boxColliderBiggest.transform.TransformPoint( _boxColliderBiggest.center );

            }

            //Debug.Log( "Bounds are: " +
            //            "\nX[ " + _boxBoundsData.m_XMin + ", " + _boxBoundsData.m_XMax + " ]" +
            //            "\nY[ " + _boxBoundsData.m_YMin + ", " + _boxBoundsData.m_YMax + " ]" +
            //            "\nZ[ " + _boxBoundsData.m_ZMin + ", " + _boxBoundsData.m_ZMax + " ]" );

            // Extract the largest bound.
            float _largestBound = 0.0f;
            ExtractLargestBound( ref _largestBound, _boxBoundsData.m_XMin, _boxBoundsData.m_XMax );
            ExtractLargestBound( ref _largestBound, _boxBoundsData.m_YMin, _boxBoundsData.m_YMax );
            ExtractLargestBound( ref _largestBound, _boxBoundsData.m_ZMin, _boxBoundsData.m_ZMax );

            //Debug.Log( "Largest bound: " + _largestBound );

            // Set camera size to dynamically fit the object.
            m_cameraCapture.SetOrthographicSize( _largestBound / 2.0f * 1.075f ); // 7.5% larger than the object.


            // Remove "(Clone)" from end of object name.
            string _Name = m_instanceObject.name;
            _Name = _Name.Replace( "(Clone)", "" );

            m_instanceObject.name = _Name;

            ResetAngleData( );
        }
    }



    private void FindBiggestCollider( ref BoxCollider a_boxColliderBiggest, BoxCollider a_boxColliderCurrent, ref float a_dimensionHighestMagnitude, ref BoxBoundsData a_boxBoundsData )
    {
        bool _biggerBox = false;

        // Need to check each dimension to ensure that the largest dimension is captured.
        CheckBoxDimension( a_boxColliderCurrent.size.x, ref a_dimensionHighestMagnitude, ref _biggerBox );
        CheckBoxDimension( a_boxColliderCurrent.size.y, ref a_dimensionHighestMagnitude, ref _biggerBox );
        CheckBoxDimension( a_boxColliderCurrent.size.z, ref a_dimensionHighestMagnitude, ref _biggerBox );


        if( _biggerBox )
        {
            // This box is bigger than all previous boxes.
            // Set this box as the root collider for the object.
            a_boxColliderBiggest = a_boxColliderCurrent;
        }

        Vector3 _boxCentreCurrent = a_boxColliderCurrent.transform.TransformPoint( a_boxColliderCurrent.center );

        // Determine if bounds have increased.
        bool _negativeValue = true;

        CheckBound( _boxCentreCurrent.x + a_boxColliderCurrent.size.x / 2.0f, ref a_boxBoundsData.m_XMax );
        CheckBound( _boxCentreCurrent.x - a_boxColliderCurrent.size.x / 2.0f, ref a_boxBoundsData.m_XMin, _negativeValue );

        CheckBound( _boxCentreCurrent.y + a_boxColliderCurrent.size.y / 2.0f, ref a_boxBoundsData.m_YMax );
        CheckBound( _boxCentreCurrent.y - a_boxColliderCurrent.size.y / 2.0f, ref a_boxBoundsData.m_YMin, _negativeValue );

        CheckBound( _boxCentreCurrent.z + a_boxColliderCurrent.size.z / 2.0f, ref a_boxBoundsData.m_ZMax );
        CheckBound( _boxCentreCurrent.z - a_boxColliderCurrent.size.z / 2.0f, ref a_boxBoundsData.m_ZMin, _negativeValue );

    }



    private void CheckBoxDimension( float a_size, ref float a_dimensionHighestMagnitude, ref bool a_biggerBox )
    {
        if( a_size > a_dimensionHighestMagnitude )
        {
            a_biggerBox = true;
            a_dimensionHighestMagnitude = a_size;
        }
    }



    private void CheckBound( float a_boundCurrent, ref float a_boundLimit, bool a_negativeValue = false )
    {
        bool _newLimit = false;
        if( a_negativeValue )
        {

            if( a_boundCurrent < a_boundLimit )
            {
                //Debug.Log( "Found a new min bound: " + a_boundCurrent + ", was: " + a_boundLimit );

                //Found higher X bound.
                _newLimit = true;
            }
        }
        else
        {
            if( a_boundCurrent > a_boundLimit )
            {
                //Debug.Log( "Found a new max bound: " + a_boundCurrent + ", was: " + a_boundLimit );

                //Found higher X bound.
                _newLimit = true;
            }
        }

        if( _newLimit )
        {
            a_boundLimit = a_boundCurrent;
        }
    }



    private void ExtractLargestBound( ref float a_largestBound, float a_boundCurrentMin, float a_boundCurrentMax )
    {
        float _boundDifference = a_boundCurrentMax - a_boundCurrentMin;

        if( _boundDifference > a_largestBound )
        {
            a_largestBound = _boundDifference;
        }
    }



    public void OnButtonPrev( )
    {
        if( m_indexItem > 0 )
        {
            m_indexItem--;

            LoadItemAtIndex( m_itemList, m_indexItem );
        }
        else
        {
            // Already at the first item: reset to zero degrees.
            ResetAngleData( );

            m_transform.rotation = Quaternion.AngleAxis( 0.0f, Vector3.up );
        }

    }



    public void OnButtonNext( )
    {
        if( m_indexItem < m_itemList.AssetReferenceCount - 1 )
        {
            m_indexItem++;

            LoadItemAtIndex( m_itemList, m_indexItem );
        }
    }



    public void OnButtonPlay( )
    {
        m_paused = !m_paused;
    }


    public void OnButtonExit( )
    {
        Stop( );
    }



    public void OnSliderValueChanged( float a_value )
    {
        m_rotationPeriod = a_value * a_value;
    }
}
